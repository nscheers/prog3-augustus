package be.kdg.examen.reflection;

import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

class CreateMockTests {

    private MyInterface itf;

    @BeforeEach
    public void setup() {
        List<ExpectedAction> expectedActions = new ArrayList<>();
        expectedActions.add(new ExpectedAction("doeA", "5", "null"));
        expectedActions.add(new ExpectedAction("doeA", "42", "null"));
        expectedActions.add(new ExpectedAction("getA", "null", "5"));
        expectedActions.add(new ExpectedAction("doeB", "Bla", "null"));
        expectedActions.add(new ExpectedAction("doeC", "null", "null"));
        expectedActions.add(new ExpectedAction("getC", "null", "A"));
        expectedActions.add(new ExpectedAction("getB", "5", "foo"));

        itf = (MyInterface) MockFactory.create(MyInterface.class, expectedActions);
    }
    
    @Test
    void testActionsInScenario() {
        itf.doeA(5);
        itf.doeA(42);
        Assert.assertEquals(5, itf.getA());
        itf.doeB("Bla");
        itf.doeC();
        Assert.assertEquals('A', itf.getC());
        Assert.assertEquals("foo", itf.getB(5));
    }

    @Test
    void testActionNotInScenario() {
        try {
            itf.getC();
        }
        catch (WrongActionException ex) {
            Assert.assertEquals("Scenario does not exist or is in wrong order", ex.getMessage());
        }
    }

    @Test
    void testActionWrongParameter() {
        try {
            itf.doeA(66);
        } catch (WrongActionException ex) {
            Assert.assertEquals("Unexpected parameter", ex.getMessage());
        }

        itf.doeA(5);
        itf.doeA(42);
        itf.getA();

        try {
            itf.doeB("BlaBla");
        } catch (WrongActionException ex) {
            Assert.assertEquals("Unexpected parameter", ex.getMessage());
        }
    }

//    @Test
//    void testActionTooManyParameter() {
//        try {
//            itf.doeA(6, 'Q');
//        } catch (WrongActionException ex) {
//            Assert.assertEquals("Maximum of one parameter is allowed", ex.getMessage());
//        }
//    }

    @Test
    void testTooManyActions() {
        itf.doeA(5);
        itf.doeA(42);
        Assert.assertEquals(
                5, itf.getA());
        itf.doeB("Bla");
        itf.doeC();
        Assert.assertEquals('A', itf.getC());
        Assert.assertEquals("foo", itf.getB(5));
        try {
            itf.doeA(5);
        } catch (WrongActionException ex) {
            Assert.assertEquals("Number of expected actions exceeded", ex.getMessage());
        }
    }

}

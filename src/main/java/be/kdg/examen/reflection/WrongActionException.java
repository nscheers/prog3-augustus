package be.kdg.examen.reflection;

public class WrongActionException extends RuntimeException {
    public WrongActionException(String message) {
        super(message);
    }
}

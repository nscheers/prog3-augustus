package be.kdg.examen.reflection;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;
import java.util.List;

public class MockFactory {
    // aan te vullen!
    public static Object create(Class itf, List<ExpectedAction> expectedActions) {
        InvocationHandler handler = new MockInvocationHandler(expectedActions);
        return Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(), new Class[]{itf}, handler);
    }
}

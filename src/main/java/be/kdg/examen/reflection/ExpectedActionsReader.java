package be.kdg.examen.reflection;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

/*
 * Dit is een utility-klasse om een scenario in te lezen vanuit een bestand
 * Je hoeft hier niets aan te veranderen
 */
public class ExpectedActionsReader {
    public static List<ExpectedAction> readActionsFromFile(String scenarioFile) {
        List<ExpectedAction> result = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(scenarioFile))) {
            String line = reader.readLine();
            int lineNumber = 1;
            while (line != null) {
                if (!line.startsWith("#")) {
                    String[] strings = line.split(" ");
                    if (strings.length != 3) throw new RuntimeException("line " + lineNumber + " does not contain 3 fields!");
                    result.add(new ExpectedAction(strings[0], strings[1], strings[2]));
                }
                line = reader.readLine();
                lineNumber++;
            }
        } catch (Exception e) {
            File file = new File(scenarioFile);
            System.err.println("Error reading file: " + file.getAbsolutePath());
            e.printStackTrace();
            System.exit(1);
        }
        return result;
    }
}

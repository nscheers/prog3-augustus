package be.kdg.examen.reflection;

// dit is een voorbeeld.  Het framework moet ook met andere interface klassen werken
// iedere methode kan hoogstens 1 parameter hebben
// enkel int, String en char moeten worden ondersteund
public interface MyInterface {
    void doeA(int i);
    void doeB(String s);
    void doeC();
    int getA();
    String getB(int a);
    char getC();
}

package be.kdg.examen.reflection;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.List;

public class MockInvocationHandler implements InvocationHandler {
    private List<ExpectedAction> expectedActions;
    private Integer counter;

    public MockInvocationHandler(List<ExpectedAction> expectedActions) {
        this.expectedActions = expectedActions;
        counter = 0;
    }

    @Override
    public Object invoke(Object o, Method method, Object[] objects) throws Throwable {

        if (expectedActions.size() <= counter)  throw new WrongActionException("Number of expected actions exceeded");

        if (!method.getName().equals(expectedActions.get(counter).getMethodName())) throw new WrongActionException("Scenario does not exist or is in wrong order");

        Object parameter = parser(expectedActions.get(counter).getParameterValue());
        if (objects != null) {
            if (objects.length > 1) throw new WrongActionException("Maximum of one parameter is allowed");

            if (objects[0] instanceof String) {
                if (!objects[0].equals(parameter)) throw new WrongActionException("Unexpected parameter");
            } else if (objects[0] != (parameter)) throw new WrongActionException("Unexpected parameter");

        }
        if (objects == null && parameter != null)
            throw new WrongActionException("Unexpected parameter");

        if (!expectedActions.get(counter).getReturnValue().equals("null")) {
            String text = expectedActions.get(counter).getReturnValue();
            counter++;
            return parser(text);
        }

        counter++;
        return null;
    }

    private Object parser (String text){
        if (text.equals("null")) return null;
        try {
            return Integer.parseInt(text);
        }
        catch (NumberFormatException ignored){
        }
        if (text.length() == 1) {return text.charAt(0);}
        return text;
    }
}

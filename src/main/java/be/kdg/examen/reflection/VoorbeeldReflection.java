package be.kdg.examen.reflection;

import java.util.List;

// dit is een voorbeeld om te laten zien hoe het framework gebruikt moet kunnen worden
public class VoorbeeldReflection {
    public static void main(String[] args) {
        // lees actions uit bestand: hier hoef je niets aan te wijzigen
        List<ExpectedAction> expectedActions = ExpectedActionsReader.readActionsFromFile("src/main/resources/MyScenario.txt");

        // maak een mock-object dat de vorige acties verwacht
        // dit moet je dus laten werken!
        MyInterface testObject = (MyInterface) MockFactory.create(MyInterface.class, expectedActions);

        // voer acties uit op het gecreëerde object
        // in dit voorbeeld wordt het scenario perfect gevolgd
        testObject.doeA(5);
        testObject.doeA(42);
        int i = testObject.getA();

        System.out.println("i = " + i);
        testObject.doeB("Bla");
        testObject.doeC();
        char c = testObject.getC();
        System.out.println("c = " + c);
        String f = testObject.getB(5);
        System.out.println("f = " + f);
    }

}

package be.kdg.examen.gedistribueerde.client;

import be.kdg.examen.gedistribueerde.communication.MessageManager;
import be.kdg.examen.gedistribueerde.communication.MethodCallMessage;
import be.kdg.examen.gedistribueerde.communication.NetworkAddress;
import be.kdg.examen.gedistribueerde.server.Server;

public class ServerStub implements Server {

    private NetworkAddress documentAddress;
    private NetworkAddress serverAddress;
    private MessageManager messageManager;
    public ServerStub(NetworkAddress serverAddress, NetworkAddress documentAddress) {
        this.serverAddress = serverAddress;
        this.documentAddress = documentAddress;
        this.messageManager = new MessageManager();
    }

    @Override
    public void convert(MyDocument document) {
        MethodCallMessage request = new MethodCallMessage(messageManager.getMyAddress(), "convert");
        request.setParameter("documentAddress", documentAddress.toString());
        messageManager.send(request, serverAddress);

        MethodCallMessage reply = messageManager.wReceive();

    }
}

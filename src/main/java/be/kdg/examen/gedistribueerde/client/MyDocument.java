package be.kdg.examen.gedistribueerde.client;

public interface MyDocument {
    String getText();
    void setText(String text);
}

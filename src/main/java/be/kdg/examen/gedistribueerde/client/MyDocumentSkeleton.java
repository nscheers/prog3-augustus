package be.kdg.examen.gedistribueerde.client;

import be.kdg.examen.gedistribueerde.communication.MessageManager;
import be.kdg.examen.gedistribueerde.communication.MethodCallMessage;
import be.kdg.examen.gedistribueerde.communication.NetworkAddress;

public class MyDocumentSkeleton implements Runnable {

    private MyDocument document;
    private MessageManager messageManager;

    public MyDocumentSkeleton(MyDocumentImpl myDocument) {
        this.document = myDocument;
        this.messageManager = new MessageManager();
    }

    public NetworkAddress getAddress() {
        return messageManager.getMyAddress();
    }

    @Override
    public void run() {
        while (true) {
            MethodCallMessage request = messageManager.wReceive();
            handleRequest(request);
        }
    }

    private void handleRequest(MethodCallMessage request) {
        if(request.getMethodName().equals("setText")) {
            document.setText(request.getParameter("text"));
            MethodCallMessage reply = new MethodCallMessage(messageManager.getMyAddress(), "reply");
            messageManager.send(reply, request.getOriginator());
        }
        else if(request.getMethodName().equals("getText")) {
            String text = document.getText();
            MethodCallMessage reply = new MethodCallMessage(messageManager.getMyAddress(), "reply");
            reply.setParameter("text", text);
            messageManager.send(reply, request.getOriginator());
        }
    }
}

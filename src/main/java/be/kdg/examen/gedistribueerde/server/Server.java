package be.kdg.examen.gedistribueerde.server;

import be.kdg.examen.gedistribueerde.client.MyDocument;

public interface Server {
    void convert(MyDocument document);
}

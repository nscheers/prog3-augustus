package be.kdg.examen.gedistribueerde.server;

import be.kdg.examen.gedistribueerde.client.MyDocument;
import be.kdg.examen.gedistribueerde.communication.MessageManager;
import be.kdg.examen.gedistribueerde.communication.MethodCallMessage;
import be.kdg.examen.gedistribueerde.communication.NetworkAddress;

public class ServerSkeleton {
    private Server server;
    private MessageManager messageManager;

    public ServerSkeleton(Server server) {
        this.server = server;
        messageManager = new MessageManager();
    }

    public void run() {
        while (true) {
            MethodCallMessage request = messageManager.wReceive();
            handleRequest(request);
        }

    }

    public NetworkAddress getAddress() {
        return messageManager.getMyAddress();
    }

    private void handleRequest(MethodCallMessage request){
        if (request.getMethodName().equals("convert")){
            String documentAddressString = request.getParameter("documentAddress");
            String[] documentAdressArray = documentAddressString.split(":");
            NetworkAddress documentAddress = new NetworkAddress(documentAdressArray[0], Integer.parseInt(documentAdressArray[1]));
            MyDocument document = new MyDocumentStub(documentAddress);
            this.server.convert(document);
            MethodCallMessage reply = new MethodCallMessage(messageManager.getMyAddress(), "reply");
            messageManager.send(reply, request.getOriginator());

        }
    }
}

package be.kdg.examen.gedistribueerde.server;

import be.kdg.examen.gedistribueerde.client.MyDocument;

public class ServerImpl implements Server {
    @Override
    public void convert(MyDocument document) {
        StringBuilder tekst = new StringBuilder(document.getText());
        for(int i=0; i<tekst.length(); i++) {
            char c = tekst.charAt(i);
            if (c<'z' && c>'a') tekst.setCharAt(i, '.');
        }
        document.setText(tekst.toString());
    }
}

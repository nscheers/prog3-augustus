package be.kdg.examen.gedistribueerde.server;

import be.kdg.examen.gedistribueerde.client.MyDocument;
import be.kdg.examen.gedistribueerde.communication.MessageManager;
import be.kdg.examen.gedistribueerde.communication.MethodCallMessage;
import be.kdg.examen.gedistribueerde.communication.NetworkAddress;

public class MyDocumentStub implements MyDocument {
    private MessageManager messageManager;
    private NetworkAddress documentAddress;

    public MyDocumentStub(NetworkAddress clientAddress) {
        messageManager = new MessageManager();
        this.documentAddress = clientAddress;

    }

    @Override
    public String getText() {
        MethodCallMessage request = new MethodCallMessage(messageManager.getMyAddress(), "getText");
        messageManager.send(request, documentAddress);
        MethodCallMessage reply = messageManager.wReceive();
        return reply.getParameter("text");
    }

    @Override
    public void setText(String text) {
        MethodCallMessage request = new MethodCallMessage(messageManager.getMyAddress(), "setText");
        request.setParameter("text", text);
        messageManager.send(request, documentAddress);
        MethodCallMessage reply = messageManager.wReceive();
    }
}
